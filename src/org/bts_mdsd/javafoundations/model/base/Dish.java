package org.bts_mdsd.javafoundations.model.base;

import java.io.Serializable;

public abstract class Dish<T extends Serializable> {
	
	protected String mDishName;	
	private boolean mGfdStatus;
	private boolean mVgdStatus;
	private boolean mHmdStatus;
	private boolean mSfdStatus;
	protected T extras;
	
	protected Dish(String mDishName, boolean mGfdStatus, boolean mVgdStatus,
			boolean mHmdStatus, boolean mSfdStatus, T extras) {
		this.mDishName = mDishName;
		this.mGfdStatus = mGfdStatus;
		this.mVgdStatus = mVgdStatus;
		this.mHmdStatus = mHmdStatus;
		this.mSfdStatus = mSfdStatus;
		this.extras = extras;
	}

	public String getDishName() { 
		return this.mDishName; 
	}
	public void setDishName(String dishName) { 
		this.mDishName = dishName; 
	}
	
	public boolean isGfdStatus() {
		return mGfdStatus;
	}
	public void setGfdStatus(boolean gfdStatus) {
		this.mGfdStatus = gfdStatus;
	}
	
	public boolean isVgdStatus() {
		return this.mVgdStatus;
	}
	public void setVgdStatus(boolean vgdStatus) {
		this.mVgdStatus = vgdStatus;
	}
	
	public boolean isHmdStatus() {
		return mHmdStatus;
	}
	public void setHmdStatus(boolean hmdStatus) {
		this.mHmdStatus = hmdStatus;
	}
	
	public boolean isSfdStatus() {
		return mSfdStatus;
	}
	public void setSfdStatus(boolean sfdStatus) {
		this.mSfdStatus = sfdStatus;
	}
	
	public abstract T getExtras();
	public abstract void setExtras(T extras);
	
	@Override
	public String toString() {
		
		StringBuilder stringBuilder = new StringBuilder()
			.append("Dish: ")
			.append(this.getDishName())
			.append(this.isGfdStatus() ? ", gluten-free" : "")
			.append(this.isVgdStatus() ? ", vegetarian" : "")
			.append(this.isHmdStatus() ? ", halal" : "")
			.append(this.isSfdStatus() ? ", seafood-free." : "; ");
		
		return stringBuilder.toString();
	}
}
